package com.bookapp.service;

import java.util.Collections;
import java.util.List;

import com.bookapp.exception.BookNotFoundException;
import com.bookapp.exception.IdNotFoundException;
import com.bookapp.model.Book;
import com.bookapp.repository.BookRepositoryImpl;
import com.bookapp.repository.IBookRepository;


public class BookServiceImpl implements IBookService {

    IBookRepository bookRepository = new BookRepositoryImpl();

   
    public List<Book> getAll() throws BookNotFoundException {
        return bookRepository.findAll();

    }

   
    public List<Book> getByAuthor(String author) throws BookNotFoundException {
        List<Book> booksByAuthor = bookRepository.findByAuthor(author);
        if (booksByAuthor != null) {
            Collections.sort(booksByAuthor, (b1, b2) -> b1.getTitle().compareTo(b2.getTitle()));
        }
        return booksByAuthor;
    }

   
    public List<Book> getByCategory(String category) throws BookNotFoundException {
        List<Book> booksByCategory = bookRepository.findByCategory(category);
        if (booksByCategory != null) {
            Collections.sort(booksByCategory, (b1, b2) -> b1.getTitle().compareTo(b2.getTitle()));
        }
        return booksByCategory;
    }

   
    public Book getById(int bookId) throws IdNotFoundException {
        return bookRepository.findById(bookId);
    }

   
    public List<Book> getByLesserPrice(double price) throws BookNotFoundException {
        List<Book> booksByPrice = bookRepository.findByLesserPrice(price);
        if (booksByPrice != null) {
            Collections.sort(booksByPrice, (b1, b2) -> b1.getTitle().compareTo(b2.getTitle()));
        }
        return booksByPrice;
    }


	@Override
	public void addingNewBook(Book book) {
		bookRepository.addBook(book);
	}


	@Override
	public void deletingBook(int id) {
		bookRepository.deleteBook(id);
	}


	@Override
	public void updatingBook(int id, double price) { 	
		bookRepository.updateBook(id, price);
	}

}
