package com.bookapp.service;

import java.util.List;

import com.bookapp.exception.BookNotFoundException;
import com.bookapp.exception.IdNotFoundException;
import com.bookapp.model.Book;


public interface IBookService {
	
	void addingNewBook(Book book);
	
	void deletingBook(int id);
	
	void updatingBook(int id, double price);
	
    List<Book> getAll() throws BookNotFoundException;

    List<Book> getByAuthor(String author) throws BookNotFoundException;

    List<Book> getByCategory(String category) throws BookNotFoundException;

    Book getById(int bookId) throws IdNotFoundException;

    List<Book> getByLesserPrice(double price) throws BookNotFoundException;
}
