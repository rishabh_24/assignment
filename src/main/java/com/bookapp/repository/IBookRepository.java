package com.bookapp.repository;

import java.util.List;

import com.bookapp.exception.BookNotFoundException;
import com.bookapp.exception.IdNotFoundException;
import com.bookapp.model.Book;


public interface IBookRepository {
	void addBook(Book book);
	
	void deleteBook(int id);
	
	void updateBook(int id, double price);
	
    List<Book> findAll() throws BookNotFoundException;

    List<Book> findByAuthor(String author) throws BookNotFoundException;

    List<Book> findByCategory(String category) throws BookNotFoundException;

    Book findById(int bookId) throws IdNotFoundException;

    List<Book> findByLesserPrice(double price) throws BookNotFoundException;
}
