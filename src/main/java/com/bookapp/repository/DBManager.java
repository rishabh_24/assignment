package com.bookapp.repository;

import org.bson.Document;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class DBManager {
	static MongoClient mongoInstance;
	
	public static void opnConnection() {
		mongoInstance = MongoClients.create("mongodb://localhost:27017");
	}
	
	public static void closeConnection() {
		mongoInstance.close();
	}
	
	public MongoDatabase getDatabase() {
		MongoDatabase database = mongoInstance.getDatabase("kloud");
		return database;
	}
	
	public MongoCollection<Document> getCollection(){
		MongoDatabase database = getDatabase();
		MongoCollection<Document> document = database.getCollection("book");
		return document;
	}
	
}
