package com.bookapp.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.bookapp.exception.BookNotFoundException;
import com.bookapp.exception.IdNotFoundException;
import com.bookapp.model.Book;
import com.google.gson.Gson;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

public class BookRepositoryImpl implements IBookRepository {

	DBManager manager = new DBManager();
	@Override
	public void addBook(Book book) {
        MongoCollection<Document> collection = manager.getCollection();
		collection.insertOne(new Document(getDocument(book)));
		System.out.println("Book has been added successfully!");
	}


	@Override
	public void deleteBook(int id) {
		MongoCollection<Document> collection = manager.getCollection();
		Document doc = new Document("_id", id);
		collection.findOneAndDelete(doc);
		System.out.println("Book with id(" + id + ") has been removed!!");
	}


	@Override
	public void updateBook(int id, double price) {  
		MongoCollection<Document> collection = manager.getCollection();
		// Document doc = new Document("_id", id);
        Bson doc = Filters.eq("id", id);
        Bson update = Updates.set("price", price);
		collection.findOneAndUpdate(doc, update);
		System.out.println("Book with id(" + id + ") has been updated!!");
	}

	
    public List<Book> findAll() throws BookNotFoundException {
    	List<Book> allBooks = showBooks();
    	if (allBooks.isEmpty()) {
            throw new BookNotFoundException("No books found!");
        }
    	return allBooks;
    }

         
    public List<Book> findByAuthor(String author) throws BookNotFoundException {
        List<Book> booksByAuthor = new ArrayList<>();
        for (Book book : showBooks()) {
            if (book.getAuthor() == author) {
                booksByAuthor.add(book);
            }
        }
        if (booksByAuthor.isEmpty()) {
            throw new BookNotFoundException("Books by" + author + " not found!");
        }
        return booksByAuthor;
    }

    @Override
    public List<Book> findByCategory(String category) throws BookNotFoundException {
        List<Book> booksByCategory = new ArrayList<>();
        for (Book book : showBooks()) {
            if (book.getCategory() == category) {
                booksByCategory.add(book);
            }
        }
        if (booksByCategory.isEmpty()) {
            throw new BookNotFoundException("Books with " + category + " not found!");
        }
        return booksByCategory;
    }

    @Override
    public Book findById(int bookId) throws IdNotFoundException {
        Book bookById = null;
        for (Book book : showBooks()) {
            if (book.getBookId() == bookId) {
                bookById = book;
                break;
            }
        }
        if (bookById == null) {
            throw new IdNotFoundException("Book with " + bookId + " not found!");
        }
        return bookById;
    }

    @Override
    public List<Book> findByLesserPrice(double price) throws BookNotFoundException {
        List<Book> booksByPrice = new ArrayList<>();
        for (Book book : showBooks()) {
            if (book.getPrice() < price) {
                booksByPrice.add(book);
            }
        }
        if (booksByPrice.isEmpty()) {
            throw new BookNotFoundException("Books lesser than " + price + " not found!");
        }
        return booksByPrice;
    }

    private List<Book> showBooks() {
    	MongoCollection<Document> collection = manager.getCollection();
    	List<Document> allBsonBooks = collection.find().into(new ArrayList<>());
        List<Book> allBooks = new ArrayList<Book>();
        for(Document book : allBsonBooks) {
        	Gson gson = new Gson();
        	Book bookObject = gson.fromJson(book.toJson(), Book.class);
        	allBooks.add(bookObject);
        }
        return allBooks;
    }
    
    private Map<String, Object> getDocument(Book book) {
		Map<String, Object> newDoc = new HashMap<>();
		newDoc.put("_id", book.getBookId());
		newDoc.put("title", book.getTitle());
		newDoc.put("author", book.getAuthor());
		newDoc.put("price", book.getPrice());
		newDoc.put("category", book.getCategory());
		return newDoc;
	}


}
