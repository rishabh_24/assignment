package com.bookapp.main;

import java.util.List;

import com.bookapp.exception.BookNotFoundException;
import com.bookapp.model.Book;
import com.bookapp.repository.DBManager;
import com.bookapp.service.BookServiceImpl;
import com.bookapp.service.IBookService;

public class Client {
    public static void main(String[] args) {
		List<Book> books;
    	IBookService bookService = new BookServiceImpl();
		DBManager.opnConnection();
		System.out.println("Connected to database!!"+ "\n" + "===================================");
    	
		System.out.println("\nCreating a chunk to play with!!"+ "\n" + "===================================");   
		for(int i = 0; i < 100; i++) {
			Book newBook = bookGenerator(i);
			bookService.addingNewBook(newBook);
		}

		System.out.println("\nReading books from MongoDB" + "\n" + "===================================");
		try {
			books = bookService.getAll();
			for(Book book : books) {
				System.out.println(book);
			}
		} catch (BookNotFoundException e) {
			System.out.println(e.getMessage());
		}

		System.out.println("\nUpadating a specific book with id as a filter!!"+ "\n" + "===================================");
		bookService.updatingBook(16, 212.88);

		System.out.println("\nDeleting a book with id as a filter!!"+ "\n" + "===================================");
       	bookService.deletingBook(17);

		System.out.println("Connection with database closed!!");
        DBManager.closeConnection();
    }
    
    static Book bookGenerator(int id) {
    	String[] authors = { "Mahesh Chand", "Jeff Prosise", "Dave McCarter", "Allen O'neill",  
    			"Monica Rathbun", "Henry He", "Raj Kumar", "Mark Prime",  
    			"Rose Tracey", "Mike Crown" }; 
    	Double price = 1000 * Math.random();
    	Integer randomAuthor = (int)(10 * Math.random());
    	return new Book(getAlphaNumericString(5), id, authors[randomAuthor], price, getAlphaNumericString(7));
    }
    
    static String getAlphaNumericString(int n)
    {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"+ "0123456789"+ "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index = (int)(AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }
}
